define(["app/main", "test/vendor/mock-ajax"], function(app) {
    var onSuccess,
        onFailure,
        request;

    beforeEach(function() {
        "use strict";
        jasmine.Ajax.install();

        onSuccess = jasmine.createSpy('onSuccess');
        onFailure = jasmine.createSpy('onFailure');
    });

    describe('should get movies by rating', function() {
        it('retrieves movies rated "R"', function() {
            app.getMoviesByRating("R").done(onSuccess).fail(onFailure);

            request = jasmine.Ajax.requests.mostRecent();

            request.response({
                status: 200,
                responseText: JSON.stringify({
                    hello: "hello there"
                })
            });

            expect(onSuccess).toHaveBeenCalled();
        });
    });
});