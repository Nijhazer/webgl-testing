// Karma configuration
// Generated on Sat Apr 26 2014 21:19:25 GMT-0700 (Pacific Daylight Time)

module.exports = function (config) {
    config.set({

        // base path that will be used to resolve all patterns (eg. files, exclude)
        basePath: '',


        // frameworks to use
        // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
        frameworks: [
            'jasmine',
            'requirejs'
        ],


        // list of files / patterns to load in the browser
        files: [
            'test-main.js',
            {
                pattern: 'src/js/vendor/**/*.js',
                included: false
            },
            {
                pattern: "src/js/app/*.js",
                included: false
            },
            {
                pattern: "test/vendor/*.js",
                included: false
            },
            {
                pattern: "test/app/*.js",
                included: false
            }
        ],


        // list of files to exclude
        exclude: [

        ],


        // preprocess matching files before serving them to the browser
        // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
        preprocessors: {
            'src/js/app/*.js': 'coverage'
        },


        // test results reporter to use
        // possible values: 'dots', 'progress'
        // available reporters: https://npmjs.org/browse/keyword/karma-reporter
        reporters: [
            'coverage',
            'progress'
        ],


        coverageReporter: {
            type : 'html',
            dir : 'build-reports/coverage/'
        },


        // web server port
        port: 9876,


        // enable / disable colors in the output (reporters and logs)
        colors: true,


        // level of logging
        // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
        logLevel: config.LOG_INFO,


        loggers: [
            {
                type: 'console'
            }
        ],


        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: false,


        // start these browsers
        // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
        browsers: [
            'PhantomJS'
        ],


        // Continuous Integration mode
        // if true, Karma captures browsers, runs the tests and exits
        singleRun: false,

        plugins: [
            "karma-requirejs",
            "karma-jasmine",
            "karma-phantomjs-launcher",
            "karma-coverage"
        ]
    });
};
