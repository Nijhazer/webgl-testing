// Configure Require.js
var require = {
    // Default load path for js files
    baseUrl: 'js/app',
    shim: {
        // --- Use shim to mix together all THREE.js subcomponents
        'threeCore': { exports: 'THREE' },
        'TrackballControls': { deps: ['threeCore'], exports: 'THREE' },
        // --- end THREE sub-components
        'detector': { exports: 'Detector' },
        'stats': { exports: 'Stats' }
    },
    // Third party code lives in js/lib
    paths: {
        // --- start THREE sub-components
        three: '../vendor/three',
        threeCore: '../vendor/three.min',
        TrackballControls: '../vendor/controls/TrackballControls',
        // --- end THREE sub-components
        detector: '../vendor/Detector',
        stats: '../vendor/stats.min',
        // Require.js plugins
        text: '../vendor/text',
        shader: '../vendor/shader',
        // Where to look for shader files
        shaders: '../shaders',

        jquery: "../vendor/jquery"
    }
};