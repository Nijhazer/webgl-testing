// Start the app
require( ['detector', 'app', 'container', "jquery"], function ( Detector, app, container, jQuery ) {
    if ( ! Detector.webgl ) {
        Detector.addGetWebGLMessage();
        container.innerHTML = "";
    }

    console.log({jquery: jQuery});

    // Initialize our app and start the animation loop (animate is expected to call itself)
    app.init();
    app.animate();
} );
